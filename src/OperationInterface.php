<?php

namespace Drupal\file_sync;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the operation entity interface.
 */
interface OperationInterface extends ConfigEntityInterface {

  /**
   * Get operation status.
   *
   * @return bool
   *   TRUE if operation is enabled, FALSE otherwise.
   */
  public function enabled(): bool;

  /**
   * Get operation plugin ID.
   *
   * @return string
   *   Operation plugin ID.
   */
  public function getPluginId(): string;

  /**
   * Get plugin settings.
   *
   * @return array
   *   Plugin settings.
   */
  public function getPluginSettings(): array;

  /**
   * Run operation on cron.
   *
   * @return bool
   *   TRUE if operation should run, FALSE otherwise.
   */
  public function cron(): bool;

}

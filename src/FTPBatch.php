<?php

namespace Drupal\file_sync;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file_sync\Event\BatchEventsInterface;
use Drupal\file_sync\Event\BatchFinishedEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Service for creating batches.
 */
class FTPBatch implements BatchInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function create(int $batch_size = 10, $params = []): void {
    /** @var \Drupal\Core\Batch\BatchBuilder $batchBuilder */
    $batchBuilder = (new BatchBuilder())
      ->setTitle($this->t('Transfering FTP files ...'))
      ->setFinishCallback([self::class, 'finishProcess'])
      ->setInitMessage('The file transfter batch created.')
      ->setProgressMessage($this->t('FTP transfer batch @current of @total'));

    $client = static::getFtpClient()->setSettingsFromPlugin($params)->connect();
    $file_list = $client->list($params['source_path']);

    $total = count($file_list);
    $file_limit = $params['file_limit'] ?? 0;
    $files_to_process = [];
    $i = 0;
    foreach ($file_list as $file) {
      $i++;
      $files_to_process[] = $file;
      if ($i == $total || !($i % $batch_size)) {
        $batchBuilder->addOperation([self::class, 'process'], [
          $files_to_process,
          $batch_size,
          $total,
          $params,
        ]);
        $files_to_process = [];
      }

      if ($file_limit && $i >= $file_limit) {
        break;
      }
    }

    batch_set($batchBuilder->toArray());
    if (PHP_SAPI === 'cli' && function_exists('drush_backend_batch_process')) {
      drush_backend_batch_process();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function process($batch_files, $batch_size, $batch_total, $batch_params, &$context): void {
    $client = static::getFtpClient()
      ->setSettingsFromPlugin($batch_params)
      ->connect();

    $destination_path = $client->resolveRealpath($batch_params['destination_path'], TRUE);

    foreach ($batch_files as $source_file) {
      $client->getFile($source_file, $destination_path . DIRECTORY_SEPARATOR . basename($source_file));
      $context['results'][] = $source_file;
    }
    $client->closeConnection();

    $processed_items = !empty($context['results']) ? count($context['results']) : $batch_size;
    $context['message'] = t('FTP files transfered %processed/%total', [
      '%processed' => $processed_items,
      '%total' => $batch_total,
    ]);

    \Drupal::logger('file_sync')->info('FTP files transfered: ' . $processed_items . '/' . $batch_total);
  }

  /**
   * {@inheritdoc}
   */
  public static function finishProcess($success, $results, array $operations): void {
    if ($success) {
      $message = t('FTP file transfer completed.');
      \Drupal::logger('file_sync')->info($message);
      \Drupal::messenger()->addMessage($message);
    }
    if (!empty($operations)) {
      \Drupal::logger('file_sync')->error('FTP file transfer failed: ' . implode(', ', $operations));
      \Drupal::messenger()->addMessage(t('FTP file transfer failed, check log for more info.'), 'error');
    }

    $dispatcher = static::getEventDispatcher();
    $event = new BatchFinishedEvent([
      'success' => $success,
      'results' => $results,
      'operations' => $operations,
    ]);
    $dispatcher->dispatch($event, BatchEventsInterface::FINISHED);
  }

  /**
   * Get FTP client.
   *
   * @return \Drupal\file_sync\FTPClientInterface
   *   The FTP client.
   */
  protected static function getFtpClient(): FTPClientInterface {
    return \Drupal::service('file_sync.ftp_client');
  }

  /**
   * Get event dispatcher.
   *
   * @return \Symfony\Component\EventDispatcher\EventDispatcherInterface
   *   The event dispatcher.
   */
  protected static function getEventDispatcher(): EventDispatcherInterface {
    return \Drupal::service('event_dispatcher');
  }

}

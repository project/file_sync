<?php

namespace Drupal\file_sync;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines entity type list builder.
 */
class OperationListBuilder extends ConfigEntityListBuilder {

  /**
   * The plugin manager.
   *
   * @var \Drupal\file_sync\OperationPluginManagerInterface
   */
  protected $operationPluginManager;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(
    ContainerInterface $container,
    EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('plugin.manager.file_sync_operation')
    );
  }

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\file_sync\OperationPluginManagerInterface $operation_plugin_manager
   *   The plugin manager.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityStorageInterface $storage,
    OperationPluginManagerInterface $operation_plugin_manager) {
    $this->entityTypeId = $entity_type->id();
    $this->storage = $storage;
    $this->entityType = $entity_type;
    $this->operationPluginManager = $operation_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    return [
      'label' => $this->t('Label'),
      'enabled' => $this->t('Enabled'),
      'plugin' => $this->t('Plugin'),
      'id' => $this->t('ID'),
    ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $plugin_id = $entity->getPluginId();
    return [
      'label' => $entity->label(),
      'enabled' => $entity->enabled() ? $this->t('Yes') : $this->t('No'),
      'plugin' => $this->operationPluginManager->getPluginList()[$plugin_id],
      'id' => $entity->id(),
    ] + parent::buildRow($entity);
  }

}

<?php

namespace Drupal\file_sync;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\FileTransfer\FTPExtension;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Simple FTP client service using the core FTPExtension.
 */
class FTPClient implements FTPClientInterface {

  use StringTranslationTrait;

  /**
   * Client settings.
   *
   * @var array
   */
  protected array $settings;

  /**
   * The FTP extension.
   *
   * @var \Drupal\Core\FileTransfer\FTPExtension
   */
  protected $ftp;

  /**
   * Current FTP connection resource.
   *
   * @var resource
   */
  protected $connection;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The stream wrapper manager.
   *
   * @var Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * Construct.
   *
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   The stream wrapper manager.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   */
  public function __construct(
    StreamWrapperManagerInterface $stream_wrapper_manager,
    FileSystemInterface $file_system) {
    $this->streamWrapperManager = $stream_wrapper_manager;
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public function connect(array $settings = []): self {
    if ($settings) {
      $this->settings = $settings;
    }

    $this->ftp = FTPExtension::factory($this->settings['jail'], $this->settings);
    $this->ftp->connect();
    $this->connection = $this->ftp->connection;

    $passive_mode = ($this->settings['advanced']['passive']) ?? FALSE;
    if ($passive_mode) {
      if (!$this->setPassiveMode()) {
        throw new \Exception('Failed to enable FTP passive mode.');
      }
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConnection() {
    if ($this->ftp === NULL) {
      throw new \Exception('No FTP connection has been initialized. Use FTPClient::connect().');
    }
    return $this->connection;
  }

  /**
   * {@inheritdoc}
   */
  public function closeConnection(): bool {
    return $this->connection ? ftp_close($this->connection) : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function list(string $directory = '.', bool $fullpath = TRUE): array {
    $list = ftp_nlist($this->getConnection(), $directory);
    if (!$list) {
      throw new \Exception('Failed to list the FTP directory: ' . $directory);
    }
    if ($fullpath) {
      foreach ($list as $key => $filename) {
        $list[$key] = $directory . DIRECTORY_SEPARATOR . $filename;
      }
    }
    return $list;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile(string $source_file, string $destination_file, $return = FALSE): ?string {
    $connection = $this->getConnection();

    // Make sure destination path exists.
    $destination_path = pathinfo($destination_file, PATHINFO_DIRNAME);
    if (!is_dir($destination_path) && !$this->fileSystem->prepareDirectory($destination_path, FileSystemInterface::CREATE_DIRECTORY)) {
      throw new \Exception('Could not create the destination path: ' . $destination_path);
    }

    // Download the file from FTP server.
    if (!@ftp_get($connection, $destination_file, $source_file, FTP_BINARY)) {
      throw new \Exception('Cannot download ' . $source_file . ' to ' . $destination_file);
    }

    if ($return) {
      // Open the downloaded temp file and read its contents.
      $file_contents = '';
      $file_resource = fopen($destination_file, 'r');
      if (!$file_resource) {
        throw new \Exception('Failed to open file "%destination" for reading.', [
          '%destination' => $destination_file,
        ]);
      }

      while (!feof($file_resource)) {
        $file_contents .= fgets($file_resource);
      }
      fclose($file_resource);

      return $file_contents;
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getDirectory(string $source, string $destination): void {
    $connection = $this->getConnection();
    $destination->resolveRealpath($destination);

    if ($source !== '.') {
      if (ftp_chdir($connection, $source) === FALSE) {
        return;
      }
      if (!(is_dir($destination)) && !mkdir($destination)) {
        throw new \Exception('Fail to create the destination directory: ' . $destination);
      }
    }

    $files = ftp_nlist($connection, $source);
    foreach ($files as $file) {
      if ($file == '.' || $file == '..') {
        continue;
      }

      $source_file = $source . DIRECTORY_SEPARATOR . $file;
      $destination_file = $destination . DIRECTORY_SEPARATOR . $file;

      // FTP extension has no efficient way to test if current file
      // is a directory. Suppress the warning message if file is hit.
      if (@ftp_chdir($connection, $file)) {
        ftp_chdir($connection, '..');
        $this->getDirectory($source_file, $destination_file);
      } else {
        ftp_get($connection, $destination_file, $source_file, FTP_BINARY);
      }
    }

    ftp_chdir($connection, '..');
    chdir('..');
  }

  /**
   * Enable FTP passive mode.
   */
  protected function setPassiveMode(): bool {
    return ftp_pasv($this->getConnection(), TRUE);
  }

  /**
   * Resolve provided internal path to realpath.
   *
   * @param string $path
   *   Path to file.
   * @param bool $create
   *   Create path if it does not exist.
   *
   * @return string
   *   Resloved realpath.
   */
  public function resolveRealpath(string $path, bool $create = FALSE): string {
    // If stream wrapper is used convert scheme to realpath.
    if ($this->streamWrapperManager->getScheme($path)) {
      $realpath = $this->fileSystem->realpath($path);
      if ($create && !is_dir($realpath)) {
        if (!$this->fileSystem->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY)) {
          throw new \Exeception('Failed to resolve the path');
        }
      }
      $path = $this->fileSystem->realpath($path);
    }
    return $path;
  }

  /**
   * Map plugin settings to client connection settings.
   *
   * @return array
   *   FTP client settings.
   */
  public static function mapClientSettingsFromPlugin(array $settings): array {
    return [
      'username' => $settings['username'],
      'password' => $settings['password'],
      'jail' => $settings['jail'],
      'advanced' => [
        'hostname' => $settings['hostname'],
        'port' => $settings['port'],
        'passive' => $settings['passive'],
      ],
    ];
  }

  /**
   * Set client settings from FTP plugin.
   *
   * @param array $settings
   *   Settings from FTP plugin.
   *
   * @return \Drupal\file_sync\FTPClientInterface
   *   FTP client.
   */
  public function setSettingsFromPlugin(array $settings): self {
    $this->settings = static::mapClientSettingsFromPlugin($settings);
    return $this;
  }

}

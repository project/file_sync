<?php

namespace Drupal\file_sync;

/**
 * Defines batch service interface.
 */
interface BatchInterface {

  /**
   * Create batch.
   *
   * @param int $batch_size
   *   Batch size.
   * @param array $params
   *   Additional batch parameters.
   */
  public function create(int $batch_size, array $params = []): void;

  /**
   * Batch operation callback.
   *
   * @param array $batch_files
   *   List of files to process in the batch.
   * @param integer $batch_size
   *   Batch size (total items in batch).
   * @param integer $batch_total
   *   Total items in queue to process.
   * @param array $batch_params
   *   Additional batch params.
   * @param array $context
   *   Batch context.
   */
  public static function process(array $batch_files, int $batch_size, int $batch_total, array $batch_params, &$context): void;

  /**
   * Bach operations 'finished' callback.
   *
   * @param bool $success
   *   TRUE if processing was successfully completed.
   * @param mixed $results
   *   Additional data passed from $context['results'].
   * @param array $operations
   *   List of operations that did not complete.
   */
  public static function finishProcess(bool $success, $results, array $operations): void;

}

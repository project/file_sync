<?php

namespace Drupal\file_sync\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\file_sync\Entity\Operation;
use Drupal\file_sync\OperationPluginManagerInterface;

/**
 * Defines the file sync configuration entity form.
 */
class OperationForm extends EntityForm implements ContainerInjectionInterface {

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The plugin manager.
   *
   * @var \Drupal\file_sync\OperationPluginManagerInterface
   */
  protected $pluginManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    MessengerInterface $messenger,
    OperationPluginManagerInterface $pluginManager) {
    $this->messenger = $messenger;
    $this->pluginManager = $pluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('plugin.manager.file_sync_operation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $form['#tree'] = TRUE;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#description' => $this->t('Operation name'),
      '#required' => TRUE,
      '#default_value' => $this->entity->label(),
    ];
    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->enabled(),
    ];
    $form['cron'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Cron job'),
      '#description' => $this->t('Indicates if operation should run on cron.'),
      '#default_value' => $this->entity->cron(),
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#source' => ['test'],
      '#machine_name' => [
        'exists' => Operation::class . '::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];
    $form['plugin_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Plugin'),
      '#required' => TRUE,
      '#options' => ['' => $this->t('- Select -')] + $this->pluginManager->getPluginList(),
      '#ajax' => [
        'event' => 'change',
        'callback' => '::getPluginSettingForm',
        'wrapper' => 'settings-form',
      ],
      '#default_value' => $this->entity->getPluginId(),
    ];
    $form['settings'] = [
      '#type' => 'fieldset',
      '#attributes' => [
        'id' => 'settings-form',
        'class' => ['hidden'],
      ],
    ];

    $plugin_id = $form_state->isRebuilding() ? $form_state->getValue('plugin_id') : $this->entity->getPluginId();
    if ($plugin_id) {
      $this->buildPluginConfigurationForm($plugin_id, $form, $form_state);
    }

    return $form;
  }

  /**
   * Build plugin configuration form.
   */
  protected function buildPluginConfigurationForm(string $plugin_id, array &$form, FormStateInterface $form_state): void {
    $plugin = $this->pluginManager->createInstance($plugin_id, ['entity' => $this->entity]);
    $form['settings']['#title'] = $this->t('Plugin settings | %label', ['%label' => $plugin->label()]);

    $plugin_form_state = SubformState::createForSubform($form['settings'], $form, $form_state);
    $form['settings']['form'] = $plugin->buildConfigurationForm($form['settings'], $plugin_form_state);

    unset($form['settings']['#attributes']['class']);
  }

  /**
   * Plugin settings form callback.
   *
   * @return array
   *   Form build array.
   */
  public function getPluginSettingForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
    return $form['settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Submit the plugin settings form handler.
    $plugin_id = $form_state->getValue('plugin_id');
    if ($plugin_id && ($plugin = $this->pluginManager->createInstance($plugin_id, ['entity' => $this->entity]))) {
      if ($plugin instanceof PluginFormInterface) {
        $plugin_form_state = SubformState::createForSubform($form['settings']['form'], $form, $form_state);
        $plugin->submitConfigurationForm($form['settings']['form'], $plugin_form_state);
      }
    }
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate the plugin settings form handler.
    $plugin_id = $form_state->getValue('plugin_id');
    if ($plugin_id && ($plugin = $this->pluginManager->createInstance($plugin_id, ['entity' => $this->entity]))) {
      if (isset($form['settings']['form']) && $plugin instanceof PluginFormInterface) {
        $plugin_form_state = SubformState::createForSubform($form['settings']['form'], $form, $form_state);
        $plugin->validateConfigurationForm($form['settings']['form'], $plugin_form_state);
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    switch ($this->entity->save()) {
      case SAVED_NEW:
        $message = $this->t('Operation "%label" created.', ['%label' => $this->entity->label()]);
        break;
      default:
        $message = $this->t('Operation "%label" saved.', ['%label' => $this->entity->label()]);
    }
    $this->messenger->addStatus($message);
    $form_state->setRedirect($this->entity->toUrl('collection')->getRouteName());
  }

}

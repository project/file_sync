<?php

namespace Drupal\file_sync\Form;

use Drupal\file_sync\BatchInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\file_sync\OperationInterface;
use Drupal\file_sync\OperationsDispatchInterface;

/**
 * Defines FTP batch form.
 */
class FTPBatchForm extends FormBase {

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The batch service.
   *
   * @var \Drupal\file_sync\BatchInterface
   */
  protected $batch;

  protected $entityTypeManager;

  protected $routeMatch;

  /**
   * The operations dispatcher.
   *
   * @var \Drupal\file_sync\OperationsDispatchInterface
   */
  protected $operationsDispatcher;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    MessengerInterface $messenger,
    BatchInterface $batch,
    EntityTypeManagerInterface $entity_type_manager,
    CurrentRouteMatch $current_route_match,
    OperationsDispatchInterface $operations_dispatcher) {
    $this->messenger = $messenger;
    $this->batch = $batch;
    $this->entityTypeManager = $entity_type_manager;
    $this->routeMatch = $current_route_match;
    $this->operationsDispatcher = $operations_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('file_sync.ftp_batch'),
      $container->get('entity_type.manager'),
      $container->get('current_route_match'),
      $container->get('file_sync.operations_dispatch'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'test_batch_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $operation = $this->loadOperation();
    $settings = $operation->getPluginSettings();

    $form['actions']['#type'] = 'actions';
    $form['description'] = [
      '#markup' => $this->t('Connect to FTP server <b>%host</b> and transfer files from <b>%source</b> to <b>%destination</b>', [
        '%host' => $settings['hostname'],
        '%source' => $settings['source_path'],
        '%destination' => $settings['destination_path'],
      ]),
    ];
    if ($settings['file_limit']) {
      $form['description']['#markup'] .= '<br>';
      $form['description']['#markup'] .= $this->t('<b>Transfer is limited to %limit of files.</b>', ['%limit' => $settings['file_limit']]);
    }
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Transfer'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $operation_id = $this->routeMatch->getParameter('operation_id');
    if ($operation_id) {
      $this->operationsDispatcher->execute(OperationsDispatchInterface::ENV_DRUPAL_BATCH, $operation_id);
    }
  }

  protected function loadOperation(): OperationInterface {
    $operation_id = $this->routeMatch->getParameter('operation_id');
    return $this->entityTypeManager->getStorage('file_sync_op')->load($operation_id);
  }

}

<?php

namespace Drupal\file_sync;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\file_sync\Event\OperationEventsInterface;
use Drupal\file_sync\Plugin\OperationPluginInterface;
use Drupal\file_sync\Event\PluginPreExecuteEvent;
use Drupal\file_sync\Event\PluginPostExecuteEvent;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Defines the operations dispatcher class.
 */
class OperationsDispatch implements OperationsDispatchInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The plugin manager.
   *
   * @var \Drupal\file_sync\OperationPluginManagerInterface
   */
  protected $pluginManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    OperationPluginManagerInterface $plugin_manager,
    EventDispatcherInterface $event_dispatcher,
    LoggerChannelFactoryInterface $logger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->pluginManager = $plugin_manager;
    $this->eventDispatcher = $event_dispatcher;
    $this->logger = $logger->get('file_sync');
  }

  /**
   * {@inheritdoc}
   */
  public function execute($environment, $operation_id = ''): void {
    $storage = $this->entityTypeManager->getStorage('file_sync_op');
    $operations = [];
    if ($operation_id) {
      $operations = $storage->loadByProperties([
        'enabled' => 1,
        'id' => $operation_id,
      ]);
    }
    else {
      $operations = $storage->loadByProperties(['enabled' => 1]);
    }

    /** @var \Drupal\file_sync\OperationInterface $operation */
    foreach ($operations as $operation) {
      if ($environment === static::ENV_DRUPAL_CRON && !$operation->cron()) {
        continue;
      }

      /** @var \Drupal\file_sync\Plugin\OperationPluginInterface $plugin */
      $plugin = $this->pluginManager->createInstance($operation->getPluginId(), ['entity' => $operation]);
      if ($plugin instanceof OperationPluginInterface) {
        $this->logger->info($this->t('Operation %label (%id) executed.', [
          '%label' => $operation->label(),
          '%id' => $operation->id(),
        ]));

        // Dispatch the event before plugin gets executed.
        $event = new PluginPreExecuteEvent($plugin);
        $this->eventDispatcher->dispatch($event, OperationEventsInterface::OPERATION_PLUGIN_PRE_EXECUTE);

        if ($plugin->getStatus()) {
          // Execute the operatoin plugin.
          $plugin->execute($operation->getPluginSettings());

          $this->logger->info($this->t('Operation %label (%id) finished.', [
            '%label' => $operation->label(),
            '%id' => $operation->id(),
          ]));

          // Dispatch the event after plugin has been executed.
          $event = new PluginPostExecuteEvent($plugin);
          $this->eventDispatcher->dispatch($event, OperationEventsInterface::OPERATION_PLUGIN_POST_EXECUTE);
        }
      }
    }
  }

}

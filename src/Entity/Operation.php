<?php

namespace Drupal\file_sync\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\file_sync\OperationInterface;

/**
 * Defines the file sync operation entity.
 *
 * @ConfigEntityType(
 *   id = "file_sync_op",
 *   label = @Translation("File sync operation"),
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_prefix = "operation",
 *   config_export = {
 *     "id",
 *     "label",
 *     "enabled",
 *     "plugin_id",
 *     "settings",
 *     "cron"
 *   },
 *   handlers = {
 *     "list_builder" = "Drupal\file_sync\OperationListBuilder",
 *     "access" = "Drupal\file_sync\OperationAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\file_sync\Form\OperationForm",
 *       "add" = "Drupal\file_sync\Form\OperationForm",
 *       "edit" = "Drupal\file_sync\Form\OperationForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer site configuration",
 *   links = {
 *     "add-form" = "/admin/config/services/file-sync/ops/add",
 *     "edit-form" = "/admin/config/services/file-sync/ops/{file_sync_op}/edit",
 *     "delete-form" = "/admin/config/services/file-sync/ops/{file_sync_op}/delete",
 *     "collection" = "/admin/config/services/file-sync/ops"
 *   }
 * )
 */
class Operation extends ConfigEntityBase implements OperationInterface {

  /**
   * {@inheritdoc}
   */
  public function enabled(): bool {
    return (bool) $this->get('enabled');
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId(): string {
    return $this->get('plugin_id') ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginSettings(): array {
    return $this->get('settings') ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function cron(): bool {
    return $this->get('cron') ?? FALSE;
  }

}

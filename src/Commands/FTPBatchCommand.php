<?php

namespace Drupal\file_sync\Commands;

use Drush\Commands\DrushCommands;
use Drupal\file_sync\OperationsDispatchInterface;

/**
 * Defines FTP drush command.
 */
class FTPBatchCommand extends DrushCommands {

  /**
   * The operation dispatcher.
   *
   * @var \Drupal\file_sync\OperationsDispatchInterface
   */
  protected $operationsDispatcher;

  /**
   * Constructor.
   *
   * @param \Drupal\file_sync\OperationsDispatchInterface $operations_dispatcher
   *   The operations dispatcher.
   */
  public function __construct(OperationsDispatchInterface $operations_dispatcher) {
    $this->operationsDispatcher = $operations_dispatcher;
  }

  /**
   * Run operations dispatcher.
   *
   * @param array $options
   *   Command options.
   *
   * @command filesync:run
   * @option operation_id Run operation by ID.
   * @usage filesync:run
   * @ussage filesync:run --operation_id=<operation-id>
   */
  public function run(array $options = ['operation_id' => NULL]) {
    $operation_id = $options['operation_id'];
    if ($operation_id) {
      $this->say('Executing operation with ID: ' . $operation_id);
      $this->operationsDispatcher->execute(OperationsDispatchInterface::ENV_DRUSH, $operation_id);
    }
    else {
      $this->say('Executing all operations ...');
      $this->operationsDispatcher->execute(OperationsDispatchInterface::ENV_DRUSH);
    }
  }

}

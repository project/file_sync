<?php

namespace Drupal\file_sync;

/**
 * Defines FTP client interface.
 */
interface FTPClientInterface {

  /**
   * Initialize the FTP connection.
   *
   * @param array $settings
   *   Connection paramteres.
   *
   * @return self
   *   Self instance.
   */
  public function connect(array $settings = []);

  /**
   * Get FTP connection resource.
   *
   * @return resource
   *   Active FTP connection resource.
   */
  public function getConnection();

  /**
   * Close FTP connection.
   *
   * @return resource
   *   TRUE if connection was closed, FALSE otherwise.
   */
  public function closeConnection(): bool;

  /**
   * Download FTP directory (recursively).
   *
   * @param string $source
   *   Source path (remote).
   * @param string $destination
   *   Destination path (local).
   */
  public function getDirectory(string $source, string $destination): void;

  /**
   * Download file from FTP server to specified destination.
   *
   * @param string $source_file
   *   Full path to the source file on the FTP server.
   * @param string $destination_file
   *   Full path to the local destination file.
   * @param bool $return
   *   TRUE if method should return the file contents, FALSE otherwise.
   *
   * @return mixed
   *   If $return is TRUE downloaded file contents, NULL otherwise.
   */
  public function getFile(string $source_file, string $destination_file, bool $return = FALSE): ?string;

  /**
   * Return list of files on the FTP server.
   *
   * NOTE: If you are having problems returning files from the
   * server set FTP passive mode {@see FTP::setPassiveMode()}.
   *
   * @param string $directory
   *   Remote directory path.
   * @param bool $fullpath
   *   Show files full path in the list.
   *
   * @return array
   *   List of files.
   */
  public function list(string $directory = '.', bool $fullpath = TRUE): array;

}

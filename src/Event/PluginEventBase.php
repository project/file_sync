<?php

namespace Drupal\file_sync\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\file_sync\Plugin\OperationPluginInterface;

/**
 * Defines the plugin execute base event.
 */
abstract class PluginEventBase extends Event {

  /**
   * Current operation plugin.
   *
   * @var \Drupal\file_sync\Plugin\OperationPluginInterface
   */
  protected $plugin;

  /**
   * {@inheritdoc}
   */
  public function __construct(OperationPluginInterface $plugin) {
    $this->plugin = $plugin;
  }

  /**
   * Get current operation plugin.
   *
   * @return \Drupal\file_sync\Plugin\OperationPluginInterface
   *   Plugin instance.
   */
  public function getPlugin(): OperationPluginInterface {
    return $this->plugin;
  }

}

<?php

namespace Drupal\file_sync\Event;

/**
 * Defines the Batch events interface.
 */
interface BatchEventsInterface {

  /**
   * The name of the event fired after Batch processing finished.
   *
   * @Event
   *
   * @see \Drupal\file_sync\Event\PluginPostExecuteEvent
   */
  const FINISHED = 'file_sync.batch.finished';

}

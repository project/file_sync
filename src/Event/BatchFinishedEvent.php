<?php

namespace Drupal\file_sync\Event;

/**
 * Defines the event executed after batch processing is finished.
 */
class BatchFinishedEvent extends BatchEventBase {}

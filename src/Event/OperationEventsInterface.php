<?php

namespace Drupal\file_sync\Event;

/**
 * Defines the operation events.
 */
interface OperationEventsInterface {

  /**
   * The name of the event fired before operaton plugin gets executed.
   *
   * @Event
   *
   * @see \Drupal\file_sync\Event\PluginPostExecuteEvent
   */
  const OPERATION_PLUGIN_PRE_EXECUTE = 'file_sync.operation_plugin_pre_execute';

/**
   * The name of the event fired after operaton plugin gets executed.
   *
   * NOTE: This event will only be executed if plugin was executed
   * (see plugin status property).
   *
   * @see \Drupal\file_sync\Plugin\OperationPluginBase::$status
   *
   * @Event
   *
   * @see \Drupal\file_sync\Event\PluginPostExecuteEvent
   */
  const OPERATION_PLUGIN_POST_EXECUTE = 'file_sync.operation_plugin_post_execute';

}

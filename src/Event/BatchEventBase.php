<?php

namespace Drupal\file_sync\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Defines the batch base event.
 */
abstract class BatchEventBase extends Event {

  /**
   * Batch processing info.
   *
   * @var array
   */
  protected array $info;

  /**
   * Constructor.
   *
   * @param array
   *   Batch processing info.
   */
  public function __construct(array $info) {
    $this->info = $info;
  }

  /**
   * Get batch processing info.
   *
   * @return array
   *  Batch processing info.
   */
  public function getInfo(): array {
    return $this->info;
  }

}

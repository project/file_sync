<?php

namespace Drupal\file_sync\Event;

/**
 * Defines the pre plugin execute event.
 */
class PluginPreExecuteEvent extends PluginEventBase {}

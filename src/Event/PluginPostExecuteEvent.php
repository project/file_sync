<?php

namespace Drupal\file_sync\Event;

/**
 * Defines the post plugin execute event.
 */
class PluginPostExecuteEvent extends PluginEventBase {}

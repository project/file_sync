<?php

namespace Drupal\file_sync;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\file_sync\Annotation\OperationPlugin;
use Drupal\file_sync\Plugin\OperationPluginInterface;

/**
 * Operation plugin manager.
 */
class OperationPluginManager extends DefaultPluginManager implements OperationPluginManagerInterface {

  /**
   * Constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/file_sync', # Folder where plugins are discovered.
      $namespaces,
      $module_handler,
      OperationPluginInterface::class,
      OperationPlugin::class
    );

    $this->setCacheBackend($cache_backend, 'file_sync_operation_plugin');
    $this->alterInfo('file_sync_operation_plugin');
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    $instance = parent::createInstance($plugin_id, $configuration);
    // Ensure plugin access to the operation entity.
    $entity = $configuration['entity'] ?? NULL;
    if (!$entity || !$entity instanceof OperationInterface) {
      throw new \Exception('Operation plugin instance requires the operation entity (' . OperationInterface::class . ') to be injected.');
    }
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginList(): array {
    $plugins = [];
    $definitions = $this->getDefinitions();
    foreach ($definitions as $definition) {
      $plugins[$definition['id']] = $definition['label'];
    }
    return $plugins;
  }

}

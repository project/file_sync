<?php

namespace Drupal\file_sync;

/**
 * Defines the operations dispatch interface.
 */
interface OperationsDispatchInterface {

  /**
   * Indicates operation is being dispatched as Drush CLI.
   *
   * @var string
   */
  const ENV_DRUSH = 'drush-cli';

  /**
   * Indicates operation is being dispatched as cronjob.
   *
   * @var string
   */
  const ENV_DRUPAL_CRON = 'drupal-cron';

  /**
   * Indicates operation is being dispatched as Drupal web batch.
   *
   * @var string
   */
  const ENV_DRUPAL_BATCH = 'drupal-batch';

  /**
   * Run dispatching of the enabled operations.
   *
   * @param string $environment
   *   Environment operation is being dispatch from.
   *   See OperationsDispatchInterface::ENV_*.
   * @param string $operation_id
   *   Execute all operations or operation by ID.
   */
  public function execute(string $environment, string $operation_id = ''): void;

}

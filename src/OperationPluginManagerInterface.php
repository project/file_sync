<?php

namespace Drupal\file_sync;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Defines the operation plugin manager.
 */
interface OperationPluginManagerInterface extends PluginManagerInterface {

  /**
   * Get plugins as list.
   *
   * @return array
   *   List of plugins ("plugin_id" => "label").
   */
  public function getPluginList(): array;

}

<?php

namespace Drupal\file_sync\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Defines operation plugin interface.
 */
interface OperationPluginInterface extends PluginInspectionInterface, PluginFormInterface, ContainerFactoryPluginInterface {

  /**
   * Execute the plugin.
   *
   * @param array $settings
   *   Settings needed for the plugin to operate.
   */
  public function execute(array $settings);

}

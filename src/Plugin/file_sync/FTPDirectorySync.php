<?php

namespace Drupal\file_sync\Plugin\file_sync;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file_sync\Plugin\OperationPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Url;
use Drupal\file_sync\BatchInterface;
use Drupal\file_sync\OperationsDispatchInterface;

/**
 * Defines plugin for downloading directory from FTP server.
 *
 * @OperationPlugin(
 *   id = "file_sync_ftp",
 *   label = "FTP - Directory sync"
 * )
 */
class FTPDirectorySync extends OperationPluginBase {

  use StringTranslationTrait;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * FTP batch.
   *
   * @var \Drupal\file_sync\BatchInterface
   */
  protected $batch;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    LoggerChannelFactoryInterface $logger,
    FileSystemInterface $file_system,
    BatchInterface $batch) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger);
    $this->fileSystem = $file_system;
    $this->batch = $batch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('file_system'),
      $container->get('file_sync.ftp_batch'),
      $container->get('file_sync.ftp_batch')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute(array $settings) {
    if ($this->getStatus()) {
      try {
        $settings = $this->getEntity()->getPluginSettings();
        if ($settings['log']) {
          $this->logger->info('FTP plugin executed.');
        }

        $this->batch->create($settings['batch_size'], $settings);

        if ($settings['log']) {
          $this->logger->info('FTP plugin execution finished.');
        }
      }
      catch (\Exception $exception) {
        $this->logger->error($exception->getMessage());
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $settings = $this->getEntity()->getPluginSettings();
    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#required' => TRUE,
      '#default_value' => $settings['username'] ?? '',
    ];
    $form['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#required' => TRUE,
      '#default_value' => $settings['password'] ?? '',
    ];
    $form['hostname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hostname'),
      '#required' => TRUE,
      '#default_value' => $settings['hostname'] ?? '',
    ];
    $form['port'] = [
      '#type' => 'number',
      '#title' => $this->t('Port'),
      '#default_value' => $settings['port'] ?? 21,
    ];
    $form['jail'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FTP jail'),
      '#description' => $this->t('Full path to path where operations are restricted to.'),
      '#required' => TRUE,
      '#default_value' => $settings['jail'] ?? '',
    ];
    $form['source_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Source path'),
      '#description' => $this->t('Full path to directory on the FTP server.'),
      '#required' => TRUE,
      '#default_value' => $settings['source_path'] ?? '',
    ];
    $form['destination_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Destination path'),
      '#description' => $this->t('Full path to directory on the local server.'),
      '#required' => TRUE,
      '#default_value' => $settings['destination_path'] ?? '',
    ];
    $form['passive'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Passive'),
      '#description' => $this->t('Use passive FTP connection.'),
      '#default_value' => $settings['passive'] ?? 1,
    ];
    $form['log'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log activity'),
      '#description' => $this->t('Create watchdog messages about FTP plugin activity.'),
      '#default_value' => $settings['log'] ?? 0,
    ];
    if (isset($settings['hostname'])) {
      $form['batch_size'] = [
        '#type' => 'number',
        '#title' => $this->t('Batch size'),
        '#description' => $this->t('Number of files to transfer per batch.'),
        '#default_value' => $settings['batch_size'] ?? 10,
      ];
      $form['file_limit'] = [
        '#type' => 'number',
        '#title' => $this->t('Transfter file limit'),
        '#description' => $this->t('Limit the number of files to be transfered. After exceeding the limit the operation will stop. <br>This is only useful for testing purposes. <strong>Defaults to "0" which means all files will be downloaded</strong>.'),
        '#default_value' => $settings['file_limit'] ?? 0,
      ];
      $form['batch'] = [
        '#type' => 'link',
        '#title' => $this->t('Transfer FTP files'),
        '#url' => Url::fromRoute('file_sync.batch_form', ['operation_id' => $this->getEntity()->id()]),
        '#attributes' => [
          'class' => [
            'button',
            'button--primary',
          ],
        ],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Check if we have permissions to write into "destination_path".
    $destination_path = $form_state->getValue('destination_path');
    if ($destination_path) {
      if (!$this->fileSystem->prepareDirectory($destination_path, FileSystemInterface::CREATE_DIRECTORY)) {
        $form_state->setErrorByName('destination_path', $this->t(
          '<strong>Destination path</strong> could not be created.'
        ));
      }
    }
  }

}

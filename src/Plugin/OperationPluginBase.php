<?php

namespace Drupal\file_sync\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\file_sync\OperationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the base class for file sync operation plugins.
 */
abstract class OperationPluginBase extends PluginBase implements OperationPluginInterface {

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a \Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger->get('file_sync');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')
    );
  }

  /**
   * Indication if plugin is ready to be executed.
   *
   * @var bool
   */
  protected bool $status = TRUE;

  /**
   * Get the plugin label.
   *
   * @return string
   *   Plugin label.
   */
  public function label(): string {
    return $this->pluginDefinition['label'];
  }

  /**
   * Get executed operation entity.
   *
   * @return \Drupal\file_sync\OperationInterface
   *   Operation entity.
   */
  public function getEntity(): OperationInterface {
    return $this->configuration['entity'];
  }

  /**
   * Get plugin status.
   *
   * @return bool
   *   TRUE if plugin should be executed, FALSE otherwise.
   */
  public function getStatus(): bool {
    return $this->status;
  }


  /**
   * Set plugin status.
   *
   * @param bool $status
   *   TRUE if plugin should be executed, FALSE otherwise.
   */
  public function setStatus(bool $status) {
    $this->status = $status;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Plugin subform is handled by the parent entity form.
    // We only extract the plugin 'settings'.
    $parent_form_state = $form_state->getCompleteFormState();
    $parent_form_state->setValue('settings',
      $form_state->getValues(['settings', 'form'])
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {}

}

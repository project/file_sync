<?php

namespace Drupal\file_sync\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines file sync operation plugin item annotation.
 *
 * @Annotation
 */
class OperationPlugin extends Plugin {

  /**
   * Unique ID for annotated class.
   *
   * @var string
   */
  public $id;

  /**
   * Plugin label.
   *
   * @var string
   */
  public $label;

}

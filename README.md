INTRODUCTION
------------

This module allows site owners perform file operations at cron run.

Operations will execute supported plugin which handles the files operation job.

INSTALLATION
------------

 - Install the module as you would normally install a contributed Drupal
   module. Visit https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

- Enable `file_sync` module.
- Go to `/admin/config/services/file-sync/ops` and add your file sync operation.
- Once operation is created and **enabled** it will run on Drupal cron.


**Override plugin settings**

Plugins may contain sensitive information which should not be exported by the config.

Use the following example in your `settings.local.php` file where
you replace the `operation_id` with your operation ID.

```php
$config['file_sync.operation.operation_id']['settings']['username'] = '';
$config['file_sync.operation.operation_id']['settings']['password'] = '';
```
